# Postcards UI

User interface for Postcards - an application for sending and receiving SMS/MMS via your Skyetel account (www.skyetel.com).

By default, Postcards UI is configured to make requests to the Postcards Backend located at `../backend/api/` - relative to the directory where the Postcards UI build is placed.

If your directory structure is different, please configure the path to the Postcards Backend in `src/Constants.js` by specifying your path in the `requestUrl` constant. 

---

In order to be able to build this project you would need node.js (https://nodejs.org) to be installed.

Before the first build it is necessary to install the project dependencies by running (in the project directory):

### `npm install`

To build the project please run: 

### `npm run build`

---

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

Please see `create-react-app-readme.md` for additional scripts and information if you would need to make any changes to the project. 