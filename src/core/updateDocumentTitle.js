import { appTitle } from "../Constants";

function updateDocumentTitle(unreadCount) {
  const title = (unreadCount ? `(${unreadCount}) ` : '') + appTitle;

  if (document.title !== title)
    document.title = title;
}

export default updateDocumentTitle;