import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';

import App from './App';
import NewTextNotifications from './core/NewTextNotifications';

import 'bootstrap/dist/css/bootstrap.css';
import 'react-image-lightbox/style.css';

import './index.scss';

ReactDOM.render(
    <App />
  , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();

NewTextNotifications.requestPermission();