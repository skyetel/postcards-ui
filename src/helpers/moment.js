/*
Just so we always import moment from one place
*/

import moment from 'moment-timezone/builds/moment-timezone-with-data-2012-2022.js';

const timezone = moment.tz.guess();

export { moment, timezone };