function bindSetState(that, state, callback) {
  return that.setState.bind(that, state, callback);
}

export { bindSetState };